const http = require('http');

const port = 4000;

// Mock database
let directory = [
	{
		firstName: "Mary Jane",
		lastName: "Dela Cruz",
		mobileNo: "09123456789",
		email: "mjdelacruz@mail.com",
		password: 123
	},
	{
		firstName: "John",
		lastName: "Doe",
		mobileNo: "09123456789",
		email: "jdoe@mail.com",
		password: 123
	}
]


const server = http.createServer((req, res) => {

	// route for returning ALL items upon receiving a GET request
	// if(req.url == '/profile' && req.method == "GET"){
	if(req.url == '/profile' && req.method == "GET"){
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.write(JSON.stringify(directory));
		res.end();
	} if(req.url == '/newItem' && req.method == "POST"){

		let requestBody = '';

		req.on('data', function(data){
			requestBody += data;
		});

		req.on('end', function(){

			requestBody = JSON.parse(requestBody);

			let newItem = {
				"firstName": requestBody.firstName,
				"lastName": requestBody.lastName,
				"mobileNo": requestBody.mobileNo,
				"email": requestBody.email,
				"password": requestBody.password
			}

			directory.push(newItem);
			console.log(directory);

			res.writeHead(200, {'Content-Type': 'application/json'});
			res.write(JSON.stringify(newItem));
			res.end();
		})

	}

});

server.listen(port);

console.log(`Server running at localhost: ${port}`);