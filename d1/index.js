const http = require("http");
const port = 4000;

const server = http.createServer((req, res) => {

	// The method "GET" means that we'll be retrieving or reading a certain info
	if(req.url == "/items" && req.method == "GET") {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end("Data retrieved from the database")
	}

	// POST method
	if(req.url == "/items" && req.method == "POST"){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end(`Data to be sent to the database`);
	}

	// PUT method
	if(req.url == "/updateItem" && req.method == "PUT") {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('Update our resources');
	}

	// DELETE method
	if()
})

server.listen(port);

console.log(`Server is running at localhost: ${port}`)